# clip2gif

Use ```ffmpeg``` to convert (a part of) a clip to an grayscale gif image.

![Demo](Alieneck.gif?raw=true)   
Demo: Alieneck.gif  400 pixel / 8 frames

Append this bash function to your *.bash_aliases* file:

```cat clip2gif_func.sh >> ~/.bash_aliases```

And source these in your *.bashrc*:
```
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

Help line:   
``` 
Fagif clip timecode [framerate{8} [size{400}]]  # generate gray animated GIF

timecode (start + duration):  [[HH:]MM:]SS[.mmm]+SSS[.mmm]
```

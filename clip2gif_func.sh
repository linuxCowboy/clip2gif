# clip2gif
#
# bash function to convert a clip to an animated grayscale gif 
#
# https://github.com/linuxCowboy/clip2gif

Fagif ()
{
	local CLIP=$1
	local START=${2/%+*}
	local LENGTH=${2/#*+}
	local RATE=${3-8}
	local SIZE=${4-400}

	local VIEWER=eom

	[[ -z $2 || $1 = "--help" || $1 = "-h" ]] &&

	echo "
	$FUNCNAME clip timecode [framerate{$RATE} [size{$SIZE}]]  # generate gray animated GIF

	timecode (start + duration):  [[HH:]MM:]SS[.mmm]+SSS[.mmm]
" && return

	[[ ! -f $1 ]] && echo "No file: $1!" && return 1

	local GRAY=${CLIP/%.???/__${START}+${LENGTH}_${SIZE}.mp4}
	local GRAY=${GRAY//:/.}  # error in ffmpeg
	local GIF=${GRAY/%.???/__${RATE}f.gif}

	if ! [[ $START =~ ^([0-9]{1,2}:){0,2}[0-9]{1,2}(\.[0-9]{0,3})?$ && $LENGTH =~ ^[0-9]{1,3}(\.[0-9]{0,3})?$ ]]; then
		echo "Invalid timecode: $2!" && return 1
	fi

	ffmpeg -hide_banner -y -ss $START -t $LENGTH -i $CLIP -filter_complex "format=gray,scale=$SIZE:-1" $GRAY
	echo

	ffmpeg -hide_banner -y -i $GRAY -f lavfi -i "movie=$GRAY,fps=$RATE,scale=flags=lanczos,palettegen[out]" \
		-filter_complex "paletteuse,fps=$RATE" $GIF
	echo

	ls -l $CLIP $GRAY $GIF

	type $VIEWER >/dev/null 2>&1 &&	$VIEWER $GIF 2>/dev/null
}
